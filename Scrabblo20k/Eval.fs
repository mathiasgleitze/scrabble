﻿module Eval

    open StateMonad

    (* Code for testing *)

    let hello = ('H', 4)::('E', 1)::('L', 1)::('L', 1)::('O', 2)::[]
    let state = mkState [("x", 5); ("y", 42)] hello ["_pos_"; "_result_"]
    let emptyState = mkState [] [] []

    let binop f a b =
        a >>= fun x ->
        b >>= fun y ->
        ret (f x y)

    let add a b = binop (+) a b
    let sub a b = binop (-) a b 
    let mul a b = binop (*) a b 
    let div a b = 
        a >>= fun x ->
        b >>= fun y ->
        if y <> 0 then ret (x / y) else fail DivisionByZero   
    let modulo a b =
        a >>= fun x ->
        b >>= fun y ->
        if y <> 0 then ret (x % y) else fail DivisionByZero  
    let aeq a b = binop (=) a b
    let alt a b = binop (<) a b 
    let conj a b = binop (&&) a b 
    let disj a b = binop (||) a b

    let isVowel (l : char) = // the tests regarding isVowel and isConsonant seem to be switched
        match System.Char.ToLower(l) with
        | 'a'| 'e' | 'i' | 'o' | 'u' | 'y' -> true
        | _ -> false

    let isConsonant (l : char) = System.Char.IsLetter(l) && isVowel(l) |> not

    type aExp =
        | N of int
        | V of string
        | WL
        | PV of aExp
        | Add of aExp * aExp
        | Sub of aExp * aExp
        | Mul of aExp * aExp
        | Div of aExp * aExp
        | Mod of aExp * aExp
        | CharToInt of cExp

    and cExp =
       | C  of char  (* Character value *)
       | CV of aExp  (* Character lookup at word index *)
       | ToUpper of cExp
       | ToLower of cExp
       | IntToChar of aExp

    type bExp =             
       | TT                   (* true *)
       | FF                   (* false *)

       | AEq of aExp * aExp   (* numeric equality *)
       | ALt of aExp * aExp   (* numeric less than *)

       | Not of bExp          (* boolean not *)
       | Conj of bExp * bExp  (* boolean conjunction *)

       | IsVowel of cExp      (* check for vowel *)
       | IsConsonant of cExp  (* check for constant *)

    let (.+.) a b = Add (a, b)
    let (.-.) a b = Sub (a, b)
    let (.*.) a b = Mul (a, b)
    let (./.) a b = Div (a, b)
    let (.%.) a b = Mod (a, b)

    let (~~) b = Not b
    let (.&&.) b1 b2 = Conj (b1, b2)
    let (.||.) b1 b2 = ~~(~~b1 .&&. ~~b2)        (* boolean disjunction *)
    let (.->.) b1 b2 = (~~b1) .||. b2           (* boolean implication *) 
       
    let (.=.) a b = AEq (a, b)   
    let (.<.) a b = ALt (a, b)   
    let (.<>.) a b = ~~(a .=. b)
    let (.<=.) a b = a .<. b .||. ~~(a .<>. b)
    let (.>=.) a b = ~~(a .<. b)                (* numeric greater than or equal to *)
    let (.>.) a b = ~~(a .=. b) .&&. (a .>=. b) (* numeric greater than *)    

    let rec arithEval a : SM<int> = 
        match a with
        | N n -> ret n
        | V v -> lookup v
        | WL -> wordLength
        | PV x -> arithEval x >>= pointValue
        | Add (b, c) -> add (arithEval b) (arithEval c)
        | Sub (b, c) -> sub (arithEval b) (arithEval c)
        | Mul (b, c) -> mul (arithEval b) (arithEval c)
        | Div (b, c) -> div (arithEval b) (arithEval c)
        | Mod (b, c) -> modulo (arithEval b) (arithEval c)
        | CharToInt c -> charEval c >>= fun x -> ret (int x)
    and charEval c : SM<char> =
        match c with
        | C x -> ret x
        | CV a -> arithEval a >>= characterValue
        | ToUpper x -> charEval x >>= (fun c -> ret (System.Char.ToUpper(c)))
        | ToLower x -> charEval x >>= (fun c -> ret (System.Char.ToLower(c)))
        | IntToChar x -> arithEval x >>= fun c -> ret (char c)
        

    let rec boolEval b : SM<bool> =
        match b with            
        | TT -> ret true                                                      (* true *)
        | FF -> ret false                                                     (* false *)
        | AEq (x, y) -> aeq (arithEval x) (arithEval y)                       (* numeric equality *)
        | ALt (x, y) -> alt (arithEval x) (arithEval y)                       (* numeric less than *)
        | Not n -> boolEval n >>= fun b -> ret(not b)                         (* boolean not *)
        | Conj (x, y) -> conj (boolEval x) (boolEval y)                       (* boolean conjunction *)
        | IsVowel v -> charEval v >>= fun r -> ret (isVowel r)                (* check for vowel *)
        | IsConsonant c -> charEval c >>= fun r -> ret (isConsonant r)        (* check for constant *)

        (*
        ((Conj (AEq (7, 0), AEq (7, 7))) .||. ( Conj (AEq (7, 7), (AEq (7, 7)) .||. AEq (7, 0))))
        *)


    type stm =                (* statements *)
    | Declare of string       (* variable declaration *)
    | Ass of string * aExp    (* variable assignment *)
    | Skip                    (* nop *)
    | Seq of stm * stm        (* sequential composition *)
    | ITE of bExp * stm * stm (* if-then-else statement *)
    | While of bExp * stm     (* while statement *)

    let rec stmntEval stmnt : SM<unit> = 
        match stmnt with
        | Declare s -> declare s
        | Ass (x, y) -> arithEval y >>= fun z -> update x z
        | Skip -> ret()
        | Seq (x, y) -> stmntEval x >>>= stmntEval y
        | ITE (x, y, z) -> push >>>= (boolEval x >>= fun b -> if b then stmntEval y else stmntEval z) >>>= pop
        | While (x, y) -> push >>>= boolEval x >>= fun b -> if b then (stmntEval y >>>= stmntEval (While (x, y))) else pop
                            
(* Part 3 (Optional) *)

    type StateBuilder() =

        member this.Bind(f, x)    = f >>= x
        member this.Return(x)     = ret x
        member this.ReturnFrom(x) = x
        member this.Delay(f)      = f ()
        member this.Combine(a, b) = a >>= (fun _ -> b)
        
    let prog = new StateBuilder()

    let arithEval2 a = failwith "Not implemented"
    let charEval2 c = failwith "Not implemented"
    let rec boolEval2 b = failwith "Not implemented"

    let stmntEval2 stm = failwith "Not implemented"

(* Part 4 (Optional) *) 

    type word = (char * int) list
    type squareFun = word -> int -> int -> Result<int, Error>

    let stmntToSquareFun stm : squareFun = 
        fun (w : word) (pos : int) (acc : int) ->
            evalSM 
                (mkState [("_pos_", pos); ("_acc_", acc); ("_result_", 0)] w ["_pos_"; "_acc_"; "_result_"]) 
                (stmntEval stm >>>= lookup "_result_")


    type coord = int * int

    type boardFun = coord -> Result<squareFun option, Error> 
    
    let getresult = 
            function
            | Success suc -> suc
            | Failure err -> failwith (sprintf "Error: %A" err)

    let stmntToBoardFun (stm : stm) (m : Map<int,Map<int, squareFun>>) =
        fun (x, y) ->
            ((evalSM 
                (mkState [("_x_", x); ("_y_", y); ("_result_", 0)] List.empty ["_x_"; "_y_"; "_result_"])
                (stmntEval stm >>>= lookup "_result_")) |>
                function
                | Success (result) -> result
                | Failure error -> failwith "Incorrect board")
                

    let evalSquare w pos acc =
        function
        | Success (Some f) -> 
          match f w pos acc with
          | Success res -> Success (Some res)
          | Failure e   -> Failure e
        | Success None -> Success None
        | Failure e        -> Failure e


    type board = {
        center        : coord
        defaultSquare : squareFun
        squares       : boardFun
    }

