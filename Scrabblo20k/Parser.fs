﻿module Ass7

open System
open FParsec
open Types
open Eval

let pAnyChar = anyChar
let letterChar = asciiLetter
let alphaNumeric = asciiLetter <|> digit
let charListToStr charList = String(List.toArray charList) |> string
let pint = pint32
let choice ps = ps |> Seq.map attempt |> choice
let (<|>) p1 p2 = attempt p1 <|> attempt p2

module ImpParser =
    let (.+.) a b = Add(a, b)
    let (.-.) a b = Sub(a, b)
    let (.*.) a b = Mul(a, b)
    let (./.) a b = Div(a, b)
    let (.%.) a b = Mod(a, b)

    let (~~) b = Not b
    let (.&&.) b1 b2 = Conj(b1, b2)
    let (.||.) b1 b2 = ~~(~~b1 .&&. ~~b2) (* boolean disjunction *)

    let (.=.) a b = AEq(a, b)
    let (.<.) a b = ALt(a, b)
    let (.<>.) a b = ~~(a .=. b) (* numeric inequality *)
    let (.<=.) a b = a .<. b .||. ~~(a .<>. b) (* numeric smaller than or equal to *)
    let (.>=.) a b = ~~(a .<. b) (* numeric greater than or equal to *)
    let (.>.) a b = ~~(a .=. b) .&&. (a .>=. b) (* numeric greater than *)


    //7.1
    let pPointValue = pstring "pointValue"

    //7.2
    let (.>*>.) p1 p2 = p1 .>> spaces .>>. p2 <?> ".>*>."
    let (.>*>) p1 p2 = p1 .>> spaces .>> p2 <?> ".>*>"
    let (>*>.) p1 p2 = p1 .>> spaces >>. p2 <?> ">*>."
    //7.3
    let parenthesise p = pchar '(' >*>. p .>*> pchar ')'
    //7.4
    let pid = letterChar <|> pchar '_' .>>. many (alphaNumeric <|> pchar '_') |>> fun (x, xs) -> x::xs |> charListToStr
    //7.6
    let binop op a b = a .>*> op .>*>. b
    //7.5
    let unop op a = op >*>. a


    let TermParse, tref = createParserForwardedToRef<aExp, unit>()
    let ProdParse, pref = createParserForwardedToRef<aExp, unit>()
    let AtomParse, aref = createParserForwardedToRef<aExp, unit>()

    let CharParse, cref = createParserForwardedToRef<cExp, unit>()

    let BoolTermParse, btref = createParserForwardedToRef<bExp, unit>()
    let BoolProdParse, bpref = createParserForwardedToRef<bExp, unit>()
    let BoolAtomParse, baref = createParserForwardedToRef<bExp, unit>()

    let StmTermParse, stref = createParserForwardedToRef<stm, unit>()
    let StmProdParse, spref = createParserForwardedToRef<stm, unit>()
    let StmAtomParse, saref = createParserForwardedToRef<stm, unit>()

    //Arethmatic
    let AddParse = binop (pchar '+') ProdParse TermParse |>> Add <?> "Add"
    let SubParse = binop (pchar '-') ProdParse TermParse |>> Sub <?> "Sub"
    do tref := choice [ AddParse; SubParse; ProdParse ] // 1 ^

    let MulParse = binop (pchar '*') AtomParse ProdParse |>> Mul <?> "Mul"
    let DivParse = binop (pchar '/') AtomParse ProdParse |>> Div <?> "Div"
    let ModParse = binop (pchar '%') AtomParse ProdParse |>> Mod <?> "Mod"
    do pref := choice [ MulParse; DivParse; ModParse; AtomParse ] // 2 ^

    let NParse = pint |>> N                                                              <?> "Int"
    let VParse = pid |>> V                                                               <?> "Var"
    let ParParse = parenthesise TermParse                                                <?> "Par"
    let NegParse = unop (pchar '-') AtomParse |>> (fun x -> Mul (N -1, x))               <?> "Neg"
    let PVParse = pPointValue >*>. AtomParse |>> PV                                      <?> "PV"
    let CharToIntParse = pstring "charToInt" >*>. parenthesise CharParse |>> CharToInt   <?> "CharToInt"
    do aref := choice [NegParse; NParse; CharToIntParse; ParParse; PVParse; VParse ] //3 ^
    //not quite sure why ParParse had to be before CharToIntParse when we have the hierarchy
    let AexpParse = TermParse

    //Character
    let Cparse = pchar '\'' >>. pAnyChar .>> pchar '\'' |>> C                            <?> "C"
    let CVParse = pstring "charValue" >*>. AtomParse |>> CV                              <?> "CV"
    let ToUpperParse = pstring "toUpper" >*>. CharParse |>> ToUpper                      <?> "ToLower"
    let ToLowerParse = pstring "toLower" >*>. CharParse |>> ToLower                      <?> "ToUpper"
    let CharParParse = parenthesise CharParse                                            <?> "CharPar"
    let IntToCharParse = pstring "intToChar" >*>. parenthesise TermParse |>> IntToChar   <?> "IntToChar"

    do cref := choice [ Cparse; CVParse; ToUpperParse; ToLowerParse; IntToCharParse; CharParParse ]
    let CexpParse = CharParse

    //Bool
    let ConjParse = binop (pstring "/\\") BoolProdParse BoolTermParse |>> Conj                          <?> "Conjunction"
    let DisjParse = binop (pstring "\\/") BoolProdParse BoolTermParse |>> (fun(x, y) -> x .||. y )      <?> "Disjunction"
    do btref := choice [ ConjParse; DisjParse; BoolProdParse ] //1 ^

    let EqualityParse = binop (pstring "=") AtomParse AtomParse |>> AEq                                 <?> "Equality"
    let LessThanParse = binop (pstring "<") AtomParse AtomParse |>> ALt                                 <?> "LessThan"
    let BiggerThanParse = binop (pstring ">") AtomParse AtomParse |>> (fun (x, y) -> x .>. y)           <?> "BiggerThan"
    let NotEqualParse = binop (pstring "<>") AtomParse AtomParse |>> (fun (x, y) -> x .<>. y)           <?> "NotEqual"
    let LessThanEqualParse = binop (pstring "<=") AtomParse AtomParse |>> (fun (x, y) -> x .<=. y)      <?> "LessThanEqual"
    let BiggerThanEqualParse = binop (pstring ">=") AtomParse AtomParse |>> (fun (x, y) -> x .>=. y)    <?> "BiggerThanEqual"
    do bpref := choice [ EqualityParse; LessThanParse; BiggerThanParse; NotEqualParse;
     LessThanEqualParse; BiggerThanEqualParse; BoolAtomParse ] //2 ^

    let TrueParse = pstring "true" |>> (fun string -> TT)                                               <?> "True"
    let FalseParse = pstring "false" |>> (fun string -> FF)                                             <?> "False"
    let NotParse = pstring "~" >*>. BoolTermParse |>> Not                                               <?> "Not"
    let BoolParParse = parenthesise BoolTermParse                                                       <?> "BoolPar"
    do baref := choice [ TrueParse; FalseParse; BoolParParse; NotParse ] //3 ^

    let BexpParse = BoolTermParse

    // 7.10
    let CurBraParse = pchar '{' >*>. StmTermParse .>*> pchar '}'//Helper

    let SeqParse =  binop (pchar ';' ) StmProdParse StmTermParse |>> Seq                                 <?> "Sequential Composition"
    do stref := choice [ SeqParse; StmProdParse ] // 1 ^

    let ITEParse = pstring "if" >*>. BoolTermParse .>*>. (pstring "then" >*>. CurBraParse) .>*>. (pstring "else" >*>. CurBraParse)
                 |>> (fun ((x, y), z) -> ITE (x, y, z))                                                  <?> "If-Then-Else"

    let ITParse = pstring "if" >*>. BoolTermParse .>*>. (pstring "then" >*>. CurBraParse)
                 |>> (fun (x, y) -> (x, y, Skip) |> ITE)                                                 <?> "If-Then"

    let WhileParse = pstring "while" >*>. BoolTermParse .>*>. (pstring "do" >*>. CurBraParse)|>> While   <?> "While"
    do spref := choice [ ITEParse; ITParse; WhileParse; StmAtomParse ] // 2 ^

    let DeclareParse = pstring "declare" >>. spaces1 >>. pid |>> Declare                                            <?> "Declare"
    let AssParse = binop (pstring ":=") pid TermParse |>> (fun (x, y) -> (x, y) |> Ass) <?> "Assign"
    do saref := choice [ DeclareParse; AssParse; CurBraParse ] // 3 ^

    let StmParse = StmTermParse


    let getParserResult s (pr : ParserResult<'a, 'b>) =
            match pr with
            | Success (t, _, _)   -> 
                 t
            | Failure (err, _, _) -> 
                let errorStr = sprintf "Failed to parse %s\n\nError:\n%A" s err
                failwith errorStr

    let runTextParser stmParse programCode =
        run stmParse programCode |> getParserResult programCode


            




