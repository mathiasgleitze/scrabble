module MultiSet 
    type MultiSet<'a when 'a : comparison> =
    | MS of Map<'a,uint32>
        override x.ToString() =
            match x with
            | MS ms -> sprintf "%s" 
                        (Map.fold (fun state key value -> 
                            state + "(" + key.ToString() + ", #" + (int value).ToString() + "), ") 
                        "{" 
                        ms)
                        |> fun string -> if (string.Length > 1) then string.Remove (string.Length - 2) + "}" else string + "}"

    let empty = MS Map.empty

    let isEmpty (MS ms) = ms.IsEmpty

    let contains a (MS ms) = ms.ContainsKey a

    let numItems a (MS ms) = 
        ms.TryFind a |> 
        function
        | Some x -> x
        | None -> uint32 0
    
    let add a n (MS ms) = numItems a (MS ms) |> fun currentN -> MS (ms.Add (a, (currentN + n)))

    let addSingle a (MS ms) = add a 1u (MS ms)

    let remove a n (MS ms) = 
        numItems a (MS ms) |>
        function
        | x when x > n -> ms.Add (a, (x - n))
        | x when x <= n -> ms.Remove a
        | _ -> failwith "ERROR"
        |> MS

    let removeSingle a (MS ms) = remove a 1u (MS ms)

    let fold f state (MS ms) = Map.fold f state ms

    let size (MS ms) = fold (fun state key value -> state + value) 0u (MS ms)

    let foldBack f (MS ms) state = Map.foldBack f ms state

    let map f (MS ms) = fold (fun state key value -> add (f key) value state) empty (MS ms)

    let ofList lst = List.fold (fun acc item -> addSingle item acc) empty lst

    let toList (MS ms) =
        let rec keyValToList key (value : uint32) =
            match value with
            |0u -> []
            |_ -> key::keyValToList key (value-1u) 
        fold 
            (fun state key value -> state @ (keyValToList key value)) 
            [] 
            (MS ms)

    let union (MS ms1) (MS ms2) =
        fold 
            (fun (MS state) key value -> 
                numItems key (MS state) 
                |> function
                   | i when i = 0u                       -> (MS state)
                   | i when i >= (numItems key (MS ms1)) -> (MS state)
                   | _                                   -> add key value (MS state)
            ) 
            (MS ms2)
            (MS ms1)

    let sum (MS ms1) (MS ms2) =
        fold 
            (fun (MS state) key value -> add key value (MS state))
            (MS ms2)
            (MS ms1)
    
    let subtract (MS ms1) (MS ms2) =
        fold 
            (fun (MS state) key value -> remove key value (MS state))
            (MS ms1)
            (MS ms2)

    let intersection (MS ms1) (MS ms2) =
        fold 
            (fun (MS state) key value -> 
                numItems key (MS ms2) 
                |> function
                   | 0u  -> (MS state)
                   | x -> add key x (MS state)
            ) 
            (empty)
            (MS ms1)