﻿module StateMonad

    type Error = 
        | VarExists of string
        | VarNotFound of string
        | IndexOutOfBounds of int
        | DivisionByZero 
        | ReservedName of string           

    type Result<'a, 'b>  =
        | Success of 'a
        | Failure of 'b

    type State = { vars     : Map<string, int> list
                   word     : (char * int) list 
                   reserved : Set<string> }

    type SM<'a> = S of (State -> Result<'a * State, Error>)

    let mkState lst word reserved = 
           { vars = [Map.ofList lst];
             word = word;
             reserved = Set.ofList reserved }

    let evalSM (s : State) (S a : SM<'a>) : Result<'a, Error> =
        match a s with
        | Success (result, _) -> Success result
        | Failure error -> Failure error

    let bind (f : 'a -> SM<'b>) (S a : SM<'a>) : SM<'b> =
        S (fun s ->
              match a s with
              | Success (b, s') -> 
                match f b with 
                | S g -> g s'
              | Failure err     -> Failure err)


    let ret (v : 'a) : SM<'a> = S (fun s -> Success (v, s))
    let fail err     : SM<'a> = S (fun s -> Failure err)

    let (>>=)  x f = bind f x
    let (>>>=) x f = x >>= (fun () -> f)

    let push = S (fun s -> Success ((), {s with vars = Map.empty :: s.vars}))

    let pop : SM<unit> =
        S (fun s -> Success ((), {s with vars = s.vars.Tail}))

    let wordLength : SM<int> = 
        S (fun s -> Success ((s.word.Length), s))    


    let characterValue (pos : int) : SM<char> = 
        S (fun s ->
            match (List.tryItem pos s.word) with
                | Some (c, i) -> Success (c, s)
                | None -> Failure (IndexOutOfBounds pos))     

    let pointValue (pos : int) : SM<int> = 
        S (fun s ->
            match (List.tryItem pos s.word) with
                | Some (c, i) -> Success (i, s)
                | None -> Failure (IndexOutOfBounds pos))


    let lookup (x : string) : SM<int> = 
        let rec aux =
            function
            | []      -> None
            | m :: ms -> 
                match Map.tryFind x m with
                | Some v -> Some v
                | None   -> aux ms

        S (fun s -> 
              match aux (s.vars) with
              | Some v -> Success (v, s)
              | None   -> Failure (VarNotFound x)) 

    let set x v = S(fun s ->
     Success ((), {s with vars = s.vars.Head.Add(x, v)::s.vars.Tail}))

    let updateMapInList var value lst index = 
        List.mapi(fun i map -> if i = index then Map.add var value map else map) lst
    

    let update (var : string) (value : int) : SM<unit> = 
        let rec aux index =
            function
            | []      -> None
            | m :: ms -> 
                match Map.tryFind var m with
                | Some v -> Some index
                | None   -> aux (index + 1) ms

        S (fun s -> 
            match aux 0 s.vars with
            | Some index   -> Success ((), {s with vars = updateMapInList var value s.vars index})
            | None         -> Failure (VarNotFound var))

    let insertElement var lst = 
      lst |> List.map (fun (x : Map<string, int>) -> if x.ContainsKey var then x else x.Add(var, 0))

    let declare (var : string) : SM<unit> = 
        S(fun s -> 
            if s.reserved.Contains var then Failure(ReservedName var) else
            match s.vars.Head.TryFind var with
            | Some v -> Failure (VarExists var)
            | None   -> Success((), {s with vars = s.vars.Head.Add(var, 0)::s.vars.Tail}))
     