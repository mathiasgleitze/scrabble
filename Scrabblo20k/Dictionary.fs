module Dictionary
    type Dictionary =
    | Leaf      of bool
    | Node      of bool * Map<char, Dictionary>
    | Alph      of Set<char> * Dictionary

    type Exists =
    | Cant
    | Does of Dictionary
    | Doesnt of Dictionary
    
    let emptyDictionary (s : string) = Alph (Set.ofArray (s.ToCharArray()), Leaf false)
    
    let chechAlph (s : string) (set : Set<char>) =
        String.forall set.Contains s 

    let rec insertWord (s : string) (d : Dictionary) =
        match d with
        | Leaf _        when s = "" -> Leaf true
        | Node (_, map) when s = "" -> Node (true, map)
        
        | Leaf b                    -> insertWord s (Node (b, Map.empty))
        | Node (b, map)             -> map.TryFind s.[0] 
                                        |> fun opt ->
                                        match opt with
                                            | Some opt -> Node (b, map.Add (s.[0], insertWord s.[1..] (opt)))
                                            | None     -> Node (b, map.Add (s.[0], insertWord s.[1..] (Leaf false)))
        | Alph (set, dic)           -> chechAlph s set |>
                                        function  
                                        | true  -> Alph (set, insertWord s dic)
                                        | false -> Alph (set, dic)   

    let rec lookupWord (s : string) (d : Dictionary) = 
        match d with
        | Leaf b        when s = "" -> b
        | Leaf _                    -> false 
        | Node (b, map) when s = "" -> b
        | Node (_, map)             -> map.TryFind s.[0] 
                                        |> fun opt ->
                                        match opt with
                                            | None     -> false
                                            | Some opt -> lookupWord s.[1..] opt
        | Alph (set, dic)           -> chechAlph s set |>
                                        function  
                                        | true  -> lookupWord s dic
                                        | false -> false

    let rec lookupChar (c : char) (d : Dictionary) = //Looks up a character and returns whether that character can be a word in the dictionary
        match d with
        | Leaf _ -> Cant
        | Node (_, map)  -> 
            map.TryFind c 
                |> fun opt ->
                match opt with
                    | None     -> Cant
                    | Some opt -> 
                        match opt with
                        | Leaf true -> Does (Leaf false)
                        | Leaf false -> Cant
                        | Node (true, map) -> Does (Node (true, map))
                        | Node (false, map) -> Doesnt (Node (false, map))
                        | _ -> failwith "Alph inside inside dictionary"
        | Alph (set, dic)           -> chechAlph (string c) set |>
                                        function  
                                        | true  -> lookupChar c dic
                                        | false -> Cant                                    

    let fromWordList alphabet wordList = 
        emptyDictionary alphabet |> List.foldBack insertWord wordList
