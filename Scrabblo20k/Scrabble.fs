(* QUICK INFO
The program does not try to put down the word with the highest score. I did not try to do this, but the board is already
parsed correctly so it should not be too hard from here.

The program only checks if it can play words placed in the order they are in the hand. I tried creating permutations of my hand
but finding plays for all of the hands takes way too long and creates a lot of dublicates.

When the program finds possible plays, it does not check if they create adjecent words and thus it will often fail
to set a word after a few rounds.

The program parses a lot of stuff that it never uses since i did not get so far. 

The algorithm for finding plays is incredibly messy and at this point even confuses me. Therefor it also has bad naming.
*)

namespace Scrabblo20k

open ScrabbleLib

open ScrabbleUtil
open ScrabbleUtil.ServerCommunication
open Eval
open Ass7.ImpParser
open Dictionary

open System.IO
open DebugPrint


module RegEx =
    open System.Text.RegularExpressions

    let (|Regex|_|) pattern input =
        let m = Regex.Match(input, pattern)
        if m.Success then Some(List.tail [ for g in m.Groups -> g.Value ])
        else None

    let parseMove ts =
        let pattern = @"([-]?[0-9]+[ ])([-]?[0-9]+[ ])([0-9]+)([A-Z]{1})([0-9]+)[ ]?" 
        Regex.Matches(ts, pattern) |>
        Seq.cast<Match> |> 
        Seq.map 
            (fun t -> 
                match t.Value with
                | Regex pattern [x; y; id; c; p] ->
                    ((x |> int, y |> int), (id |> uint32, (c |> char, p |> int)))
                | _ -> failwith "Failed (should never happen)") |>
        Seq.toList

 

module State = 
    // Make sure to keep your state localised in this module. It makes your life a whole lot easier.
    // Currently, it only keeps track of your hand, and your player numer but it could, potentially, 
    // keep track of other useful
    // information, such as number of players, player turn, etc.


    type boardState = {
        center       : coord                                    
        usedSquare   : Map<int, squareFun>
        boardFunc    : (coord -> int)
        squares      : Map<int, Map<int, squareFun>>
        playedTiles  : Map<coord, uint32 * char * int>
    }
    type state = {
        playerNumber : int
        players      : int
        playerTurn   : int
        hand         : MultiSet.MultiSet<uint32>
        boardState   : boardState
    }
    let mkBoardState center usedSquare brdFunc squares pldTiles = {
        center       = center;
        usedSquare   = usedSquare;
        boardFunc    = brdFunc;
        squares      = squares;
        playedTiles  = pldTiles;
    }
    let mkState plrNumber plrs plrTurn hand brdState = {
        playerNumber = plrNumber;
        players      = plrs;
        playerTurn   = plrTurn;
        hand         = hand;
        boardState   = brdState;
    }
    let newBoardState (board : boardProg) =
            let parsedProg = runTextParser StmParse board.prog
            let mappedStuff = Map.map (fun key map -> Map.map (fun key t -> stmntToSquareFun (runTextParser StmParse t)) map) board.squares
            let brdFun = (stmntToBoardFun parsedProg mappedStuff) 
            mkBoardState board.center Map.empty brdFun mappedStuff Map.empty

    let newState plrNumber plrs plrTurn hand = mkState plrNumber plrs plrTurn hand

    type squarePosition =
        | UsedSquare of uint32 * char * int
        | UnusedSquare of int
        | Hole

    let query (bs : boardState) (c : coord) : squarePosition =
        match bs.playedTiles.TryFind c with
        | Some x -> UsedSquare x
        | None -> match bs.boardFunc c with
                  | y when y >= 0 -> UnusedSquare y
                  | _ -> Hole

    let querySquares (bs : boardState) (i : int) =
        match bs.squares.TryFind i with
        | Some x -> x
        | None -> failwith "Element not in squares"


    let updateHand  (move : (coord * (uint32 * (char * int)))list) newTiles (st : state) =
                    (List.fold (fun acc (_, (tileid, _)) -> MultiSet.removeSingle tileid acc) st.hand move)
                    |> fun handwTilesRemoved -> 
                        debugPrint "Hand updated\n"
                        (List.fold (fun acc (tileid, amount) -> MultiSet.add tileid amount acc) handwTilesRemoved newTiles)
                    |> fun newHand -> mkState st.playerNumber st.players st.playerTurn newHand st.boardState

    let updatePlayedTiles (move : (coord * (uint32 * (char * int)))list) (st : state) =
                    List.fold (fun acc (coord, (id, (ch, i))) -> Map.add coord (id, ch, i) acc) st.boardState.playedTiles move
                    |> fun updatedTiles -> 
                        debugPrint "Board updated\n"
                        mkBoardState st.boardState.center st.boardState.usedSquare st.boardState.boardFunc st.boardState.squares updatedTiles
                    |> fun boardState -> mkState st.playerNumber st.players st.playerTurn st.hand boardState

    let updateTurn (st: state) = 
        match st.playerTurn < st.players with
        | true -> mkState st.playerNumber st.players (st.playerTurn + 1) st.hand st.boardState
        | false -> mkState st.playerNumber st.players 1 st.hand st.boardState

    let myTurn st = (st.playerNumber.Equals st.playerTurn)

    let playerNumber st  = st.playerNumber
    let hand st          = st.hand


module AI =
    open State
    open Dictionary

    type Direction = 
        | Right
        | Down
        | DownRight



    let findStartPositions (handSize :int) (st :state) = 
        let insideBounds crd = 
            match query st.boardState crd with
            | Hole -> false
            | _ -> true

        let addRight ((x : int), (y :int)) = 
            match insideBounds (x, y) with
                | true -> 
                    match query st.boardState (x-1, y) with
                    | UsedSquare _ -> None
                    | _ -> Some ((x,y), Right)
                | false -> None

        let addDown ((x : int), (y :int)) = 
            match insideBounds (x, y) with
                | true -> 
                    match query st.boardState (x, y-1) with
                    | UsedSquare _ -> None
                    | _ -> Some ((x,y), Down)
                | false -> None

        let addPositions s ((x : int), (y :int)) =
            let rights = Seq.fold 
                            (fun acc i ->  addRight (x-i, y) |> function
                                                            | Some x -> Set.add x acc
                                                            | None -> acc) 
                            Set.empty 
                            {0..handSize}
            let downs = Seq.fold 
                            (fun acc i ->  addDown (x, y-i) |> function
                                                            | Some x -> Set.add x acc
                                                            | None -> acc) 
                            Set.empty  
                            {0..handSize}
            Set.union (Set.union rights downs) s

        if (st.boardState.playedTiles.IsEmpty) then
            Set.ofList [(st.boardState.center, Down);(st.boardState.center, Right)]
         else
            Map.fold 
                (fun acc key _ -> addPositions acc key)
                Set.empty
                st.boardState.playedTiles

    let findPlays (dicty :Dictionary) (st : state) pieces =
        let incrmntCrd crd =
            function
            | Right -> (fst crd + 1, snd crd)
            | Down -> (fst crd, snd crd + 1)
            | x -> failwith (sprintf "Cant increment %A" x)

        let handToTileList (pieces :Map<uint32, tile>) (hand :MultiSet.MultiSet<uint32>) =
            MultiSet.map (fun x -> (x, Set.toList (Map.find x pieces) ) ) hand
            |> MultiSet.toList

        let addWordWords word words = 
            match words with
            | Some w -> Some (word::w)
            | None -> Some ([word])

        let something (hand :(uint32 * (char * int) list) list) crd dir dicty = 
            let startIsUsedSquare =  
                match query st.boardState crd with
                | UsedSquare _ -> true
                | UnusedSquare _ -> crd.Equals st.boardState.center //can always set a word on center square if it is a startposition
                | Hole -> failwith "A hole can't be a start position"
            let unusedSquareHaveBeenHit = not startIsUsedSquare

            let rec somethingElse (hand :(uint32 * (char * int) list) list) crd dir dicty (word :(coord * (uint32 * (char * int))) list) (words :((coord * (uint32 * (char * int))) list list) option) = 
                if (hand.IsEmpty && (query st.boardState (incrmntCrd crd dir)).Equals UsedSquare ) then 
                    words  // if the hand is empty and the next tile is not a UsedSquare there can be created no more words
                else
                    let somethingThird hand word words (crd, id, c, i) = 
                        match lookupChar c dicty with
                            | Cant -> words //Should stop
                            | Does dictyrest ->  //Add word and continue
                                match query st.boardState crd, query st.boardState (incrmntCrd crd dir) with // if the next is a used square dont add the word
                                |  _, UsedSquare _ -> 
                                    let newWord = word @ [(crd, (id, (c, i)))] 
                                    somethingElse hand (incrmntCrd crd dir) dir dictyrest newWord words
                                | UsedSquare _, UnusedSquare _ -> 
                                    if (startIsUsedSquare) then 
                                        if (unusedSquareHaveBeenHit) then
                                            let newWord = word @ [(crd, (id, (c, i)))] 
                                            somethingElse hand (incrmntCrd crd dir) dir dictyrest newWord (addWordWords newWord words)
                                        else
                                            let newWord = word @ [(crd, (id, (c, i)))] 
                                            somethingElse hand (incrmntCrd crd dir) dir dictyrest newWord words
                                    else
                                        let newWord = word @ [(crd, (id, (c, i)))] 
                                        somethingElse hand (incrmntCrd crd dir) dir dictyrest newWord (addWordWords newWord words)
                                | UnusedSquare _, UnusedSquare _ -> 
                                    if (startIsUsedSquare) then 
                                        let newWord = word @ [(crd, (id, (c, i)))] 
                                        somethingElse hand (incrmntCrd crd dir) dir dictyrest newWord (addWordWords newWord words)
                                    else 
                                        let newWord = word @ [(crd, (id, (c, i)))] 
                                        somethingElse hand (incrmntCrd crd dir) dir dictyrest newWord words
                                | _ -> 
                                    let newWord = word @ [(crd, (id, (c, i)))] 
                                    somethingElse hand (incrmntCrd crd dir) dir dictyrest newWord (addWordWords newWord words)
                            | Doesnt dictyrest -> 
                                somethingElse hand (incrmntCrd crd dir) dir dictyrest (word @ [(crd, (id, (c, i)))]) words
                    
                    match query st.boardState crd with
                    | UsedSquare (id, c, i) -> 
                        somethingThird hand word words (crd, id, c, i)
                    | UnusedSquare i -> 
                        somethingThird hand.Tail word words (hand.Head |> fun (x, y) -> (crd, x, fst y.Head, snd y.Head))
                    | Hole -> words
            somethingElse hand crd dir dicty [] None

        let findAllPlays hand startPositions = 
            Set.fold 
                (fun acc (crd, dir) -> (something hand crd dir dicty) |>
                                       function
                                       | Some x -> x::acc
                                       | None -> acc)
                []
                startPositions


        let hand = handToTileList pieces st.hand
        let startPositions = findStartPositions (int (MultiSet.size st.hand)) st
        findAllPlays hand startPositions
        
        
    let playPlay (play :((coord * (uint32 * (char * int))) list)) (brdSt :boardState) = 
            List.filter 
                (fun (crd, (id, (ch, i))) -> query brdSt crd |>
                                             function
                                             | UnusedSquare _ -> true
                                             | UsedSquare _ -> false
                                             | Hole -> failwith "Shouldn't be holes in plays") 
                play

module Print =
    open State
    open AI
    open System.Threading

    let printHand pieces hand =
        hand |>
        MultiSet.fold (fun _ x i -> forcePrint (sprintf "%d -> (%A, %d)\n" x (Map.find x pieces) i)) ()
    Thread.Sleep(200)

    let printSquares (bs : boardState)=
        for y in -8..8 do
                for x in -8..8 do
                    (query bs (x, y)) |>
                        function
                        | UnusedSquare sqr -> 
                            match sqr with
                            | 0 -> printf "  "
                            | 1 -> printf "a "
                            | 2 -> printf "b "
                            | 3 -> printf "c "
                            | 4 -> printf "d "
                            | _ -> printf "? "
                        | UsedSquare (_,c,_) -> printf "%c " c
                        | Hole -> printf "%s " "#"
                printfn ""
        Thread.Sleep(200)

    let printStarts (starts : (coord * Direction) Set) (bs : boardState) =
        let startsMap = Set.fold 
                            (fun acc (crd, dir) -> if (Map.containsKey crd acc) then (acc.Add (crd, DownRight)) else (acc.Add (crd, dir))) 
                            Map.empty
                            starts
        for y in -8..8 do
                for x in -8..8 do
                    (query bs (x, y)) |>
                        function
                        | Hole -> printf "%s " "#"
                        | _ -> 
                            match Map.tryFind (x, y) startsMap with
                            | Some x -> 
                                match x with
                                | Right -> printf "%s " "→"
                                | Down -> printf "%s " "↓"
                                | DownRight -> printf "%s " "↘"
                            | None -> printf "%s " " "
                printfn ""
        Thread.Sleep(200)


    let printPlays (plays : ((coord * (uint32 * (char * int))) list list) list) =
        List.iter (fun play -> printfn "play: %A" play) plays

    let printCheckStringDictionary diction (s :string) = 
        let rec printCheckCharDictionary diction (s :string) =
            if (s.Length = 0) then
                printfn ""
               else
                match (Dictionary.lookupChar s.[0] diction) with
                | x -> x |>
                        function
                        | Does dic -> 
                            printfn "%c Does" s.[0]
                            printCheckCharDictionary dic s.[1..]
                        | Doesnt dic -> 
                            printfn "%c Doesnt" s.[0]
                            printCheckCharDictionary dic s.[1..]
                        | Cant -> 
                            printfn "%c Cant\n" s.[0]
        printCheckCharDictionary diction s

        let printCheckCharDictionaryInner diction (s :string) = 
            for x in [1..(s.Length - 1)] do 
                let s' = (s.Remove x)
                match (Dictionary.lookupWord s' diction) with
                | true -> printfn "%s true" s'
                | false -> printfn "%s false" s'
            match (Dictionary.lookupWord s diction) with
            | true -> printfn "%s true" s
            | false -> printfn "%s false" s

        printCheckCharDictionaryInner diction s
        Thread.Sleep(200)

module Scrabble =
    open System.Threading
    open Print
    open AI
    open RegEx

    let playGame cstream pieces (diction : Dictionary) (st : State.state) =

        let rec aux (st : State.state) =
            //Thread.Sleep(5000) // only here to not confuse the pretty-printer. Remove later.


            // remove the force print when you move on from manual input (or when you have learnt the format
            if (State.myTurn st) then
            //  Print.printSquares st.boardState
            //  Thread.Sleep(200)
            //  printfn "Starts--------------------------"
            //  Print.printStarts (AI.findStartPositions 7 st) st.boardState
            //  Thread.Sleep(200)
            //  printfn "Start postions:\n%A" (AI.findStartPositions 7 st)
            //  Thread.Sleep(200)
            //  printfn "HAND----------------------------"
            //  Print.printHand pieces (State.hand st)
            //  Thread.Sleep(200)
            //  printfn "PLAYS---------------------------"
            //  let plays' = findPlays diction st pieces
            //  Print.printPlays plays'
            //  Thread.Sleep(200)

             let plays = findPlays diction st pieces
             if plays.IsEmpty then 
                send cstream (SMPass)
             else
                 let move = playPlay plays.Head.Head st.boardState
                 debugPrint (sprintf "Player %d -> Server:\n%A\n" (State.playerNumber st) (SMPlay move)) // keep the debug lines. They are useful.
                 send cstream (SMPlay move)
            

            let msg = recv cstream
            debugPrint (sprintf "Player %d <- Server:\n%A\n" (State.playerNumber st) msg) // keep the debug lines. They are useful.

            match msg with
            | RCM (CMPassed i) -> 
                (* your idiot of a enemy passed*)
                debugPrint "Enemy passed\n"
                let st' = st |> State.updateTurn// This state needs to be updated
                aux st'
            | RCM (CMPlaySuccess(ms, points, newPieces)) ->
                (* Successful play by you. Update your state (remove old tiles, add the new ones, change turn, etc) *)
                let st' = State.updateHand ms newPieces st |> State.updatePlayedTiles ms |> State.updateTurn
                debugPrint "MY MOVE\n"
                aux (st')
            | RCM (CMPlayed (pid, ms, points)) ->
                (* Successful play by other player. Update your state *)
                let st' = State.updatePlayedTiles ms st |> State.updateTurn
                debugPrint "ENEMY MOVE\n"
                aux (st')
            | RCM (CMPlayFailed (pid, ms)) ->
                (* Failed play. Update your state *)
                let st' = st // This state needs to be updated
                aux st'
            | RCM (CMGameOver _) -> ()
            | RCM a -> failwith (sprintf "not implmented: %A" a)
            | RGPE err -> printfn "Gameplay Error:\n%A" err; aux st


        aux st

    let startGame 
            (boardP : boardProg) 
            (alphabet : string) 
            (words : string list) 
            (numPlayers : uint32) 
            (playerNumber : uint32) 
            (playerTurn  : uint32) 
            (hand : (uint32 * uint32) list)
            (tiles : Map<uint32, tile>)
            (timeout : uint32 option) 
            (cstream : Stream) =
        debugPrint 
            (sprintf "Starting game!
                      number of players = %d
                      player id = %d
                      player turn = %d
                      hand =  %A
                      timeout = %A\n\n" numPlayers playerNumber playerTurn hand timeout)

        debugPrint (sprintf "SQUARES: %A\n" boardP.squares)

          
        let handSet = List.fold (fun acc (x, k) -> MultiSet.add x k acc) MultiSet.empty hand
        let boardState = State.newBoardState boardP
        let state = (State.newState (int playerNumber) (int numPlayers) (int playerTurn) handSet) boardState
        let diction = fromWordList alphabet words

        fun () -> playGame cstream tiles diction state 